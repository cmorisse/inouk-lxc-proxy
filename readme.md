Inouk LXC Proxy
===============

A dynamic reverse proxy to map URL to LXC Containers.

Given:

	cmorisse@srv.mydomain.net:~$ lxc-ls -f
	NAME           STATE     IPV4        IPV6  AUTOSTART
	----------------------------------------------------
	dev-cu1      STOPPED     -           -     NO
	dmig-acme    RUNNING     10.0.3.149  -     YES
	dev-acme     RUNNING     10.0.3.112  -     YES
	test-acme    RUNNING     10.0.3.87   -     YES
	test-jdoe    RUNNING     10.0.3.17   -     YES
	dev-jdoe     RUNNING     10.0.3.15   -     YES

inouk-lxc-proxy dynamically maps URL and serve them from containers private IPs and ports:

	URL                                      => served from host / port
	http://w-8069.dmig-acme.srv.mydomain.net => http://10.0.3.149:8069
	http://w-80.test-jdoe.srv.mydomain.net   => http://10.0.3.17:80

No need to:

- publish ports using iptables
- and/or update proxy config and restart / reload

__Inouk LXC Proxy dynamically resolves containers and ports ; just
create a container, let users launch any web server in it and directly open them in a web browser.__

__Inouk LXC Proxy is just a dev tool for now. You MUST NOT use it with production servers.__

_Inouk LXC Proxy is outrageously inspired from codebox.io behavior._

## Install
### On Linux (tested with Ubuntu)
__With a sudo account:__

	sudo apt-get install nodejs nodejs-legacy npm
	npm install -g inouk-lxc-proxy

## Launch

Use inouk-lxc-proxy for usage hint.

    glint@eye:~$ inouk-lxc-proxy
    inouk-lxc-proxy.js

    Usage:
      inouk-lxc-proxy.js start [options]

      Launch proxy and start map URL to Containers.

      options:
      -p number, --listen-port=number             Port the proxy must listen to (default=8080)
      -d number, --cache-retention-period=number  State refresh period for STOPPED
                                                  and NONEXISTENT containers (default=120s)

Use inouk-lxc-proxy __start__ to launch it.

	glint@eye:~$ inouk-lxc-proxy start
	inouk-lxc-proxy.js
	(c) 2014 Cyril MORISSE - @cmorisse
	Proxy running on port: 8080 with cache retention period set to 120 seconds.


Use command line options to adjust listen port and (lxc-ls) cache retention period.

## Deployment

Use:

- Nodejitsu __forever__ to run it continously. See [https://github.com/nodejitsu/forever](https://github.com/nodejitsu/forever) 
- Ubuntu __authbind__ to serve on port 80 with a non root user.

### forever (to run continously)

__With a sudo account:__

	sudo npm install forever -g
	
__ With the user account owner of the LXC Containers:__

    forever start $(which inouk-lxc-proxy) start
	forever list

### authbind (to serve on port 80 with a non root user)

__With a sudo account:__

	# replace with the name of your containers owner
	export INOUK_LXC_PROXY_USER=tristounet 
	
	sudo apt-get install -y authbind
	# Configuration
	sudo touch /etc/authbind/byport/80
	sudo chown $INOUK_LXC_PROXY_USER /etc/authbind/byport/80
	sudo chmod 755 /etc/authbind/byport/80

__ With the user account owner of the LXC Containers:__

	# Now to launch inouk-lxc-proxy on port 80:
    authbind --deep forever start $(which inouk-lxc-proxy) start -p 80
	
	# dont forget to go a little bit further and study forever options,
	# log file and commands:
	# forever stop $(which inouk-lxc-proxy)
	# forever list


##License
Inouk LXC Proxy is licensed under AGPL 3.0.

##Roadmap (not necessarily in this order)

- Add HTTPS support
- Add tests
- Stress and test it !!!!
- Add a customized security mecanism/callback to allow filtering on (Eg. throttling, source ip tests) 
- Setup "real" logging
- Add websocket support
- Generalize to proxy to remote host and not only containers (using a callback parameter to route)


## References

- [https://thomashunter.name/blog/using-authbind-with-node-js/](https://thomashunter.name/blog/using-authbind-with-node-js/)
- [http://manpages.ubuntu.com/manpages/trusty/man1/authbind.1.html](http://manpages.ubuntu.com/manpages/trusty/man1/authbind.1.html)

#Disclaimer:
Inouk LXC Proxy is my first node.js project. Don't hesitate to send some feedback.



