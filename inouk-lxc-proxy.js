#!/usr/bin/env node
/*
  inouk-lxc-proxy.js
  A dynamic reverse proxy to map URL to LXC Containers
  Copyright (c) 2014 Cyril MORISSE - @cmorisse

 This file is part of inouk-lxc-proxy.

 inouk-lxc-proxy.js is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 inouk-lxc-proxy.js is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with inouk-lxc-proxy.js  If not, see <http://www.gnu.org/licenses/>.

*/

var CACHE_RETENTION_PERIOD = 120;   // in seconds
var PROXY_LISTEN_PORT = 8080;       // Don't forget that 80 is a system port !

var util = require('util'),
    colors = require('colors'),
    _ = require('lodash'),
    http = require('http'),
    httpProxy = require('http-proxy'),
    child_process = require('child_process'),
    argv = require('minimist')(process.argv.slice(2));

/*
 * Command Line arguments processing
 */
util.puts("inouk-lxc-proxy.js");
if (!_.contains(argv._, 'start')) {
    util.puts("");
    util.puts("Usage:");
    util.puts("  inouk-lxc-proxy.js "+"start".blue+" [options]");
    util.puts("");
    util.puts("  Launch proxy and start map URL to Containers.");
    util.puts("");
    util.puts("  options:");
    util.puts("  -p number, --listen-port=number             Port the proxy must listen to (default=8080)");
    util.puts("  -d number, --cache-retention-period=number  State refresh period for STOPPED ");
    util.puts("                                              and NONEXISTENT containers (default=120s)");
    util.puts("");
    process.exit(126);
} else {
    // process --listen-port options
    var listenPortOption,
        listenPort;
    listenPortOption = argv.p || argv['listen-port'];
    if (listenPortOption) {
        listenPort = parseInt(listenPortOption);
        if(isNaN(listenPort)) {
            util.puts("ERROR: invalid value for -p or --listen-port option: ".red + listenPortOption.red);
            process.exit(126)
        }
        PROXY_LISTEN_PORT = listenPort;
    }

    // process --cache-retention-period options
    var cacheRetentionPeriodOption,
        cacheRetentionPeriod;
    cacheRetentionPeriodOption = argv.d || argv['cache-retention-period'];
    if (cacheRetentionPeriodOption) {
        cacheRetentionPeriod = parseInt(cacheRetentionPeriodOption);
        if(isNaN(cacheRetentionPeriod)) {
            util.puts("ERROR: invalid value for -p or --cache-retention-period option: ".red + cacheRetentionPeriodOption.red);
            process.exit(126)
        }
        CACHE_RETENTION_PERIOD = cacheRetentionPeriod;
    }

    util.puts("(c) 2014 Cyril MORISSE - @cmorisse");
    util.puts("Proxy running on port: ".green+PROXY_LISTEN_PORT.toString().magenta
        +" with cache retention period set to: ".green+CACHE_RETENTION_PERIOD.toString().magenta+" seconds.".green)
}

/*
 * containerList is an object populated with lxc-ls command result
 * Example:
 {
    'dev-oev8': {
        address: '10.0.3.133',
        state: 'STOPPED',       // RUNNING, STOPPED, NONEXISTENT
        epoch: 1399618925974    // Timestamp this entry has been resolved
    },
    'rcskljdk': {
        address: '',
        state: 'NONEXISTENT',
        epoch: 1399619001725
    }
 };
 * UNEXISTENT and STOPPED entries are refreshed every CACHE_RETENTION_PERIOD
 */

// Let's start with an empty list
var containersList = {};

function updateContainersList(name, callback) {
    /*
     * Use lxc-ls -f to update containersList and pass the container
     * object for name to the callback.
     */
    console.log("updateContainersList("+name+") called");
    lxc_ls = child_process.exec('lxc-ls -f', function(error, stdout, stderr) {
        if(error) {
            console.log('error: ' + error);
            containersList[name] = {
                address: '',
                state: 'NONEXISTENT',
                epoch: new Date().getTime()
            };
        } else {
            var containerLines = stdout.split('\n').slice(2).filter(function(p){
                return p != null && p.length>0
            });
            var containerValues = containerLines.map(function(line){
                return line.split(' ').filter(function(p){
                    return p!=null && p.length>0
                });
            });

            // update containersList from containerValues
            _.forEach(containerValues, function(e){
                containersList[e[0]] = {
                    state: e[1],
                    address: e[2] != '-' ? e[2] : '',
                    epoch: new Date().getTime()
                };
                return true;
            });

            // If there is still no container named 'name' add an UNEXISTENT one
            if(!containersList[name]){
                containersList[name] = {
                    address: '',
                    state: 'NONEXISTENT',
                    epoch: new Date().getTime()
                }
            }
        }
        // pass the container object to callback
        callback(containersList[name]);
    });
}

/*
 * search for a container in containersList
 * if not found update containersList with an
 * NONEXISTENT entry.
 */
function lookupContainer(name, callback) {
    var container = containersList[name],
        now = new Date().getTime();

    if(container) {
        // Refresh cache for not running containers
        if(_.contains(['STOPPED', 'NONEXISTENT'], container.state) && (container.epoch + CACHE_RETENTION_PERIOD * 1000) < now) {
            updateContainersList(name, callback);
        } else
            callback(container);
    } else
        updateContainersList(name, callback);
}

/*
 * Extract target host name and port from URL and pass them to callback
 * in a targetInfo object.
 */
function extractHostAndPort(req, callback) {
    /*
     * Given a request with a host URL structured as:
     *    web-portNumber.containerName.domain.ext
     * return {
     *      requestedHost: requestedHost,
     *      host: container.address,
     *      requestedPort: web-portNumber,
     *      port: portNumber,
     *      target: 'http://'+container.address+':'+portNumber
     * }
     * TODO: Check URL structure & correctness
     */
    var r = req.headers.host.split('.'),
        requestedPort = r[0].split('-')[1],
        port = isNaN(parseInt(requestedPort)) ? null : requestedPort,
        requestedHost = r[1];

    lookupContainer(requestedHost, function(container){
        callback({
            requestedHost: requestedHost,
            host: container.address,
            requestedPort: requestedPort,
            port: port,
            target: 'http://'+container.address+':'+port
        });
    });
}

/*
 * Return an error poge with a minimum information about the error.
 */
function errorCatcher(err, req, res) {

    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write("<h1>Inouk LXC Router</h1>");

    extractHostAndPort(req, function(targetInfo){
        // We can query containersList as we are always called after lookupContainer()
        var container = containersList[targetInfo.requestedHost];

        if(container.state=="NONEXISTENT") {
            res.write("Container <b>'"+targetInfo.requestedHost+"'</b> does not exist !");

        }else if(container.state=="STOPPED") {
            res.write("<p>Container <b>'"+targetInfo.requestedHost+"'</b> is STOPPED !</p>");

        }else if(container.state=="RUNNING") {
            res.write("<p>Internal Error: Failed to connect to existing running server ! </p>");
        }

        if(!targetInfo.port) {
            res.write("<p>Invalid port number: <b>'"+targetInfo.requestedPort+"'</b> !<br/> Port must be specified using w-9999 where 9999 is the requested port number.</p>");
        }else if(err.code=="ECONNREFUSED") {
            res.write("<p>Failed to connect on port: <b>'"+targetInfo.requestedPort+"'</b> of Container <b>'"+targetInfo.requestedHost+"'</b> !<br/> Are you sure there is a service listening this port on server ?</p>");
        }
        res.end();
    });
}

var proxy = httpProxy.createServer();
proxy.on('error', errorCatcher);

function inoukLXCRouter(req, res) {
    /*
     * On each request, extract container name and port from url
     * and proxy the request to the container.
     */
    extractHostAndPort(req, function(targetInfo){
        // We pass all requests to proxy.web and let it process
        // errors that we catch in errorCatcher()
        console.log("Proxying '"+req.headers.host+"' ==> '"+targetInfo.target+"'");
        proxy.web(req, res, targetInfo);
    });
}

// GO !
http.createServer(inoukLXCRouter).listen(PROXY_LISTEN_PORT);